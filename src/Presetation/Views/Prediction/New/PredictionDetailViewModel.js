import { useRef, useState } from "react";
import Image from "../../../components/shared/Image";
import { GetPrediction } from "../../../../Domain/UseCase/Prediction/GetPrediction"
import TextField from "../../../components/shared/TextField";

export default function PredictionDetailViewModel() {
    
    const [content, setContent] = useState ({
        image : <></>,
        prediction : <></>
    })

    const hiddenInputFile = useRef()
    
    async function selectFile(hiddenInputFile) {
        hiddenInputFile.current.click()
    }

    async function renderPrediction(event, prop) {
        const file = event.target.files[0]
        const url = URL.createObjectURL(file)
        const image = <Image image={url} classImage="image" imageType="image"></Image>
        setContent({ ...content, [prop]: image })

        const formData = creatFormData(file)
        var prediction = await GetPrediction(formData)
        var result = <TextField name={prediction}></TextField>
        setContent({ ...content,  [prop]: image, "prediction": result })
    }

    function creatFormData(file) {        
        const formData = new FormData();
        formData.append("file", file)
           
        return formData;
    }

    return {
        ...content,
        hiddenInputFile,
        selectFile,
        renderPrediction
    };
}

        