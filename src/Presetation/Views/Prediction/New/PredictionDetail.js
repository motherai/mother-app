import React from "react";
import usePredictionDetailViewModel from "./PredictionDetailViewModel"
import SelectFileItem from "../../../components/Prediction/SelectFileItem";
import "../../../styles/icons/Icons.css";
import "../../../styles/images/Images.css";
import "../../../styles/result/Result.css";

export default function PredictionDetail() {
    
    const {image, prediction, hiddenInputFile, selectFile, renderPrediction} = usePredictionDetailViewModel();

    return (
        <div class = "container">
            <div class="icons">
                <SelectFileItem action={() => selectFile(hiddenInputFile)} />
            </div>
            <input type="file" style={{ "display": "none" }} onChange={(event) => renderPrediction(event, "image")} ref={hiddenInputFile} />
            <div class="images"> 
                {image}
            </div>
            <div class="result"> 
                {prediction}
            </div>            
        </div>
    );    
}