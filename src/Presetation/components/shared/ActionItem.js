import React, { Component } from "react";
import TextField from "./TextField";

export default class ActionItem extends Component {
    render() {
        const typeClass = "item-name"
        return (
            <div class="item" tabIndex="0" onClick={this.props.action}>
                {this.props.classIcon}
                <TextField typeClass={typeClass} bold={this.props.bold} name={this.props.name}></TextField>
            </div>
        );
    }
}