import React, { Component } from "react";
import TextField from "./shared/TextField";

export default class Footer extends Component {
    render() {
        const title = "Trabajo de fin de máster en Inteligencia Artificial y Data Science"
        const presentan = "PRESENTAN:"
        const sofia = "Sofía Llorente García"
        const clara = "Clara Palomar Sevillano"
        const miquel = "Miquel Castells Rubio"
        const director = "DIRECTOR:"
        const teno = "Teno González"
        const typeClass = "App-text"
        const separator = ","
        return (
            <footer className="App-footer">
                <TextField typeClass={typeClass} bold={title}></TextField>
                <TextField typeClass={typeClass} name={presentan}></TextField>
                <TextField typeClass={typeClass} bold={sofia}></TextField>
                <TextField name={separator}></TextField>
                <TextField typeClass={typeClass} bold={clara}></TextField>
                <TextField name={separator}></TextField>
                <TextField typeClass={typeClass} bold={miquel}></TextField>
                <TextField typeClass={typeClass} name={director}></TextField>
                <TextField typeClass={typeClass} bold={teno}></TextField>
            </footer>
      );       
    }
}
