import React, { Component } from "react";
import logo from '../resources/shared/images/logo/logo.png'
import TextField from "./shared/TextField";
import Image from "./shared/Image";

export default class Header extends Component {
    render() {
        const title = "SISTEMAS DE DETECCIÓN DE CÁNCER DE MAMA"
        const classImage = "App-logo"
        const imageType = "logo"
        const typeClass = "App-text"
        return (
            <header className="App-header">
                <Image image={logo} classImage={classImage} imageType={imageType}></Image>
                <p className="App-project-name">
                    <TextField typeClass={typeClass} name={title}></TextField> 
                </p>
            </header>
        );
    }
}