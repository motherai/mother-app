import React, { Component } from 'react'
import SelectFileIcon from './SelectFileIcon';
import ActionItem from '../shared/ActionItem';

export default class SelectFileItem extends Component {
    render() {
        const name = "Selección"
        const bold = "ImagenHistopatológica"
        const classIcon = <SelectFileIcon/>       
        return (
            <ActionItem bold={bold} name={name} classIcon={classIcon} action={this.props.action}/>           
        );
    }
}

