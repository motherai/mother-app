import Header from './Presetation/components/Header';
import Footer from './Presetation/components/Footer';
import PredictionDetail from './Presetation/Views/Prediction/New/PredictionDetail';
import "./Presetation/styles/shared/App.css";

function App() {
  return (
    <div className="App">
      <Header/>
      <PredictionDetail/>
      <Footer/>
    </div>
  )
}

export default App;
