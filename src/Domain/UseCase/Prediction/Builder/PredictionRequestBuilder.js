
export function BuildRequest(data) {
    const request = {
        method: 'POST',
        headers : {
            'Access-Control-Allow-Origin': true
        },
        body: data
    };
    return request;
}