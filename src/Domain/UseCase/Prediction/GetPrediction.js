import { RUTE_POST_PREDICT_IDC_LOCAL } from "../../constants";
import { BuildRequest } from "./Builder/PredictionRequestBuilder"

export async function GetPrediction(idc) {
    const POSITIVE = 1
    const STR_POSITIVE = "Positivo"
    const STR_NEGATIVE = "Negativo"
    const MSG = "Predicción IDC del paciente: "
    var response = ""
    const request = BuildRequest(idc)
          
    await fetch(RUTE_POST_PREDICT_IDC_LOCAL, request)
        .then(response => response.json())
        .then(data =>  response = data);
    

    var result = response.result === POSITIVE ? STR_POSITIVE : STR_NEGATIVE    

    return MSG+result;
}