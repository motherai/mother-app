# mother-cnn

A service that provides a prediction of breast cancer based on a convolutional neural network trained with a set of invasive ductal carcinoma images.

## Getting started

To make it easy for you to get started with mother-cnn, here's a list of recommended next steps.

Create and upload the idc data/models on your blob space from azure.
https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-python?tabs=environment-variable-windows

Containers needed:
    models: sequential_idc.pt
    data: idc_data.pkl, idc_test_data.pkl

```
cd working_repo
git clone https://gitlab.com/motherai/mother-app.git
git clone https://gitlab.com/motherai/mother-cnn.git

cd mother-app
npm start

cd mother-cnn
run.py

```
## Installation

1. Python v3.10.5
2. Nodejs v16.15.1
3. Install requirements.txt
4. Configure the Azure Blob Space
5. Run the app

## Authors and acknowledgment
MIQUEL CASTELLS RUBIO, SOFÍA LLORENTE GARCÍA, CLARA PALOMAR SEVILLANO

## License
MIT License

Copyright (c) 2022 Miquel Castells Rubio, Sofía Llorente García, Clara Palomar Sevillano

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

